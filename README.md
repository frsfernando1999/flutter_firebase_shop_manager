# Shop Management

This app was made to manage the flutter_firebase_shop application.

It was made using Bloc as a state manager and used Firebase streams to manage real time orders sent by clients to the firebase using StreamBuilder.

Both applications were used to study asynchronous functions and firebase requisitions. 
