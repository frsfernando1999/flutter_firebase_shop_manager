import 'dart:async';

class LoginValidators {
  final validadeEmail =
      StreamTransformer<String, String?>.fromHandlers(handleData: (email, sink) {
    if (email.contains('@')) {
      sink.add(email);
    } else {
      sink.add(null);
      sink.addError('Insira um email válido');
    }
  });

  final validadePass =
      StreamTransformer<String, String?>.fromHandlers(handleData: (pass, sink) {
    if (pass.length >= 6) {
      sink.add(pass);
    } else {
      sink.add(null);
      sink.addError('Senha inválida, deve conter mais de 6 caracteres');
    }
  });

}
