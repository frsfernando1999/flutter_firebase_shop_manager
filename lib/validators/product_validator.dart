class ProductValidator {
  String? validateImages(List? images) {
    if (images == null || images.isEmpty) return 'Adicione imagens do produto!';
    return null;
  }

  String? validateTitle(String? text) {
    if (text == null || text.isEmpty) return 'Preencha o título do produto!';
    return null;
  }

  String? validateDescription(String? text) {
    if (text == null || text.isEmpty) return 'Preencha a descrição do produto!';
    return null;
  }

  String? validateSize(List? sizes) {
    if (sizes == null || sizes.isEmpty) return 'Insira um tamanho!';
  }

  String? validatePrice(String? text) {
    double? price = double.tryParse(text!);
    if (price != null) {
      if (!text.contains('.') || text.split('.')[1].length != 2) {
        return 'Utilize 2 casas decimais';
      } else if (price < 0) {
        return 'Coloque um valor maior que 0!';
      }
    } else {
      return 'Preço inválido';
    }
    return null;
  }
}
