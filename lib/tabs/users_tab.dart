import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:gerencia_loja/blocs/user_bloc.dart';
import 'package:gerencia_loja/tiles/users_tab_tiles/user_tile.dart';

class UsersTab extends StatelessWidget {
  const UsersTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final _userBloc = BlocProvider.getBloc<UserBloc>();

    return Column(
      children: [
        TextField(
          onChanged: _userBloc.onChangedSearch,
          style: const TextStyle(color: Colors.white),
          decoration: const InputDecoration(
            hintText: 'Pesquisar',
            hintStyle: TextStyle(color: Colors.white),
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            border: InputBorder.none,
          ),
        ),
        Expanded(
          child: StreamBuilder<List>(
              stream: _userBloc.outUsers,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.pinkAccent),
                    ),
                  );
                } else if (snapshot.data?.length == 0) {
                  return const Center(
                    child: Text(
                      'Nenhum Usuário encontrado',
                      style: TextStyle(
                        color: Colors.pinkAccent,
                      ),
                    ),
                  );
                } else {
                  return ListView.separated(
                      itemBuilder: (context, index) {
                        return UserTile(user: snapshot.data?[index]);
                      },
                      separatorBuilder: (context, index) {
                        return const Divider(
                          color: Colors.white60,
                        );
                      },
                      itemCount: snapshot.data!.length);
                }
              }),
        ),
      ],
    );
  }
}
