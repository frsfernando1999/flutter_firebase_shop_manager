import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class ImageSourceSheet extends StatelessWidget {
  const ImageSourceSheet({this.onImageSelected, Key? key}) : super(key: key);

  final Function(File)? onImageSelected;

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      onClosing: () {},
      builder: (context) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextButton(
            onPressed: () async {
              XFile? imgFile =
                  await ImagePicker().pickImage(source: ImageSource.camera);
                imageSelected(imgFile);
              },
            child: const Text(
              'Câmera',
            ),
          ),
          TextButton(
            onPressed: () async {
              XFile? imgFile =
              await ImagePicker().pickImage(source: ImageSource.gallery);
              imageSelected(imgFile);
            },
            child: const Text(
              'Galeria',
            ),
          ),
        ],
      ),
    );
  }

  void imageSelected(XFile? imgFile) async{
    if(imgFile != null){
      File file = File(imgFile.path);
      File? croppedImage = await ImageCropper().cropImage(sourcePath: file.path,
      aspectRatio: const CropAspectRatio(ratioX: 1.0, ratioY: 1.0)
      );
      onImageSelected!(croppedImage!);
    }
  }
}
