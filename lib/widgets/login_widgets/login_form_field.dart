import 'package:flutter/material.dart';

class LoginFormField extends StatelessWidget {
  const LoginFormField(
      {required this.icon,
      required this.hint,
      required this.pass,
      required this.blocStream,
      required this.onChanged,
      Key? key})
      : super(key: key);

  final IconData icon;
  final String hint;
  final bool pass;
  final Stream<String?> blocStream;
  final Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: blocStream,
        builder: (context, snapshot) {
          return TextFormField(
            onChanged: onChanged,
            obscureText: pass,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
            decoration: InputDecoration(
              errorText: snapshot.hasError ? snapshot.error.toString() : null,
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              border: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              labelText: hint,
              labelStyle: const TextStyle(
                color: Colors.white,
              ),
              icon: Icon(
                icon,
                color: Colors.white,
              ),
            ),
          );
        });
  }
}
