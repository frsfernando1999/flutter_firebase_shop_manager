import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class UserTile extends StatelessWidget {
  final Map<String, dynamic> user;

  const UserTile({required this.user, Key? key}) : super(key: key);

  final textStyle = const TextStyle(color: Colors.white);

  @override
  Widget build(BuildContext context) {
    if (user.containsKey('money')) {
      return ListTile(
        contentPadding: EdgeInsets.zero,
        title: Text(
          user['name'],
          style: textStyle,
        ),
        subtitle: Text(
          user['email'],
          style: textStyle,
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              'Pedidos: ${user['orders']}',
              style: textStyle,
            ),
            Text('Total Gasto: R\$${user['money'].toStringAsFixed(2)}',
                style: textStyle)
          ],
        ),
      );
    } else {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: 200,
              height: 20,
              child: Shimmer.fromColors(
                  child: Container(
                    color: Colors.white.withAlpha(50),
                    margin: const EdgeInsets.symmetric(vertical: 4),
                  ),
                  baseColor: Colors.white,
                  highlightColor: Colors.grey),
            ),
            SizedBox(
              width: 50,
              height: 20,
              child: Shimmer.fromColors(
                  child: Container(
                    color: Colors.white.withAlpha(50),
                    margin: const EdgeInsets.symmetric(vertical: 4),
                  ),
                  baseColor: Colors.white,
                  highlightColor: Colors.grey),
            )
          ],
        ),
      );
    }
  }
}
