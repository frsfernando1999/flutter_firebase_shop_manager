import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gerencia_loja/widgets/orders_tab_widgets/order_header.dart';

class OrderTile extends StatelessWidget {
  OrderTile({required this.order, Key? key}) : super(key: key);

  final DocumentSnapshot order;

  final states = [
    '',
    'Em preparação',
    'Em transporte',
    'Aguardado entrega',
    'Entregue'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          key: Key(order.id),
          initiallyExpanded: order['status'] != 4,
          title: Text(
            '#${order.id}: '
            '${states[order['status']]}',
            style: TextStyle(
              color: order['status'] != 4 ? Colors.grey[850] : Colors.green,
            ),
          ),
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: 16, right: 16, top: 0, bottom: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  OrderHeader(order: order),
                  const Divider(
                    color: Colors.grey,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: order['products'].map<Widget>((p) {
                      return ListTile(
                        title: Text(
                          p['product']['title'] + ' ' + p['size'],
                          style: const TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        subtitle: Text(
                          p['category'] + '/' + p['pid'],
                          style: const TextStyle(
                            fontSize: 12,
                          ),
                        ),
                        trailing: Text(
                          p['quantity'].toString(),
                          style: const TextStyle(fontSize: 16),
                        ),
                        contentPadding: EdgeInsets.zero,
                      );
                    }).toList(),
                  ),
                  const Divider(
                    color: Colors.grey,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () {
                          FirebaseFirestore.instance
                              .collection('users')
                              .doc(order['clientId'])
                              .collection('orders')
                              .doc(order.id)
                              .delete();
                          order.reference.delete();
                        },
                        child: const Text(
                          'Excluir',
                          style: TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: order['status'] > 1
                            ? () {
                                order.reference
                                    .update({'status': order['status'] - 1});
                              }
                            : null,
                        child: Text(
                          'Regredir',
                          style: TextStyle(
                            color: Colors.grey[850],
                          ),
                        ),
                        style: TextButton.styleFrom(
                          primary: Colors.grey[850],
                        ),
                      ),
                      TextButton(
                        onPressed: order['status'] < 4
                            ? () {
                                order.reference
                                    .update({'status': order['status'] + 1});
                              }
                            : null,
                        child: const Text(
                          'Avançar',
                          style: TextStyle(
                            color: Colors.blue,
                          ),
                        ),
                        style: TextButton.styleFrom(
                          primary: Colors.blue,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
