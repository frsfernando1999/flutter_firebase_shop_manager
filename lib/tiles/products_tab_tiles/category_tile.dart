import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gerencia_loja/pages/product_page.dart';
import 'package:gerencia_loja/widgets/product_page_widgets/edit_category_dialog.dart';

class CategoryTile extends StatelessWidget {
  const CategoryTile({required this.doc, Key? key}) : super(key: key);

  final DocumentSnapshot? doc;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          leading: GestureDetector(
            onTap: () {
              showDialog(
                context: context,
                builder: (context) => EditCategoryDialog(
                  category: doc,
                ),
              );
            },
            child: CircleAvatar(
              backgroundImage: NetworkImage(
                doc?['icon'],
              ),
              backgroundColor: Colors.transparent,
            ),
          ),
          title: Text(
            doc?['title'],
            style:
                TextStyle(color: Colors.grey[850], fontWeight: FontWeight.w500),
          ),
          children: [
            FutureBuilder<QuerySnapshot>(
                future: doc?.reference.collection('itens').get(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  } else {
                    return Column(
                      children: snapshot.data!.docs.map((document) {
                        return ListTile(
                             leading: CircleAvatar(
                              backgroundImage:
                                  NetworkImage(document['images'][0]),
                              backgroundColor: Colors.transparent,
                          ),
                          title: Text(document['title']),
                          trailing: Text(
                              'R\$${document['preco'].toStringAsFixed(2)}'),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ProductScreen(
                                      categoryId: doc?.id,
                                      product: document,
                                    )));
                          },
                        );
                      }).toList()
                        ..add(ListTile(
                          leading: const CircleAvatar(
                            backgroundColor: Colors.transparent,
                            child: Icon(
                              Icons.add,
                              color: Colors.pinkAccent,
                            ),
                          ),
                          title: const Text('Adicionar'),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    ProductScreen(categoryId: doc?.id)));
                          },
                        )),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
