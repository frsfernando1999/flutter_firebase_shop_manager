import 'package:flutter/material.dart';
import 'package:gerencia_loja/blocs/login_bloc.dart';
import 'package:gerencia_loja/pages/home_page.dart';
import 'package:gerencia_loja/widgets/login_widgets/login_form_field.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _loginBloc = LoginBloc();

  @override
  void initState() {
    super.initState();
    _loginBloc.outState.listen((state) {
      switch (state) {
        case LoginState.success:
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const HomePage()));
          break;
        case LoginState.fail:
          showDialog(context: context, builder: (context) => const AlertDialog(
            title: Text('Erro',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),),
            content: Text('Usuário ou senha inválidos.'),
          ));
          break;
        case LoginState.idle:
        case LoginState.loading:
      }
    });
  }

  @override
  void dispose() {
    _loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<LoginState>(
          stream: _loginBloc.outState,
          builder: (context, snapshot) {
            switch (snapshot.data) {
              case LoginState.loading:
                return const Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.pinkAccent),
                  ),
                );
              case LoginState.success:
              case LoginState.idle:
              case LoginState.fail:
                return Container(
                  alignment: Alignment.center,
                  color: Colors.black,
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(16),
                    child: Form(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const Icon(
                            Icons.store,
                            size: 170,
                            color: Colors.redAccent,
                          ),
                          LoginFormField(
                            icon: Icons.person_outline_sharp,
                            hint: 'Usuário',
                            pass: false,
                            onChanged: _loginBloc.changeEmail,
                            blocStream: _loginBloc.outEmail,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          LoginFormField(
                            icon: Icons.lock_outline_sharp,
                            hint: 'Senha',
                            pass: true,
                            onChanged: _loginBloc.changePass,
                            blocStream: _loginBloc.outPass,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          StreamBuilder<bool>(
                              stream: _loginBloc.outSubmitValid,
                              builder: (context, snapshot) {
                                return SizedBox(
                                  height: 50,
                                  child: ElevatedButton(
                                    style: ButtonStyle(backgroundColor:
                                        MaterialStateProperty.resolveWith<
                                            Color>((Set<MaterialState> states) {
                                      if (states
                                          .contains(MaterialState.disabled)) {
                                        return Theme.of(context)
                                            .colorScheme
                                            .primary
                                            .withOpacity(0.3);
                                      } else {
                                        return Theme.of(context)
                                            .colorScheme
                                            .primary;
                                      }
                                    })),
                                    onPressed:
                                        snapshot.hasData && snapshot.data!
                                            ? _loginBloc.submit
                                            : null,
                                    child: const Text(
                                      'Entrar',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                );
                              }),
                        ],
                      ),
                    ),
                  ),
                );
              default:
                return Container();
            }
          }),
    );
  }
}
