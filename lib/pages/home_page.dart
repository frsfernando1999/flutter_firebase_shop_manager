import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:gerencia_loja/blocs/orders_bloc.dart';
import 'package:gerencia_loja/blocs/user_bloc.dart';
import 'package:gerencia_loja/tabs/orders_tab.dart';
import 'package:gerencia_loja/tabs/products_tab.dart';
import 'package:gerencia_loja/tabs/users_tab.dart';
import 'package:gerencia_loja/widgets/product_page_widgets/edit_category_dialog.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController? _pageController;
  int _page = 0;

  late UserBloc _userBloc;
  late OrdersBloc _ordersBloc;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    _userBloc = UserBloc();
    _ordersBloc = OrdersBloc();
  }

  @override
  void dispose() {
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        bottomNavigationBar: _buildBottomNavigationbar(),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: BlocProvider(
            blocs: [
              Bloc((i) => _userBloc),
              Bloc((i) => _ordersBloc),
            ],
            dependencies: const [],
            child: PageView(
              controller: _pageController,
              onPageChanged: (p) {
                setState(() {
                  _page = p;
                });
              },
              children: const [
                UsersTab(),
                OrdersTab(),
                ProductsTab(),
              ],
            ),
          ),
        ),
        floatingActionButton: _buildFloating(),
      ),
    );
  }

  Widget? _buildFloating() {
    switch (_page) {
      case 0:
        return null;
      case 1:
        return SpeedDial(
          child: const Icon(Icons.sort),
          backgroundColor: Colors.pinkAccent,
          overlayOpacity: 0.4,
          overlayColor: Colors.black,
          children: [
            SpeedDialChild(
                child: const Icon(Icons.arrow_downward, color: Colors.pinkAccent,),
                backgroundColor: Colors.white,
                label: 'Concluídos Abaixo',
                labelStyle: const TextStyle(fontSize: 14),
                onTap: (){
                  _ordersBloc.setOrderCriteria(SortCriteria.concluded_last);
                }
            ),
            SpeedDialChild(
                child: const Icon(Icons.arrow_upward, color: Colors.pinkAccent,),
                backgroundColor: Colors.white,
                label: 'Concluídos Acima',
                labelStyle: const TextStyle(fontSize: 14),
                onTap: (){
                  _ordersBloc.setOrderCriteria(SortCriteria.concluded_first);
                }
            ),
          ],
        );
      case 2:
        return FloatingActionButton(
          child: Icon(Icons.add),
            backgroundColor: Colors.pinkAccent,
            onPressed: (){
              showDialog(
                context: context,
                builder: (context) => EditCategoryDialog(),
              );
            },
        );
    }
  }

  _buildBottomNavigationbar() {
    return BottomNavigationBar(
      currentIndex: _page,
      onTap: (p) {
        _pageController?.animateToPage(p,
            duration: const Duration(milliseconds: 750),
            curve: Curves.easeInOutQuad);
      },
      backgroundColor: Theme.of(context).primaryColor,
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Clientes',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.shopping_cart),
          label: 'Pedidos',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          label: 'Produtos',
        ),
      ],
      selectedLabelStyle: const TextStyle(
        fontSize: 16,
        color: Colors.white,
      ),
      selectedItemColor: Colors.blueGrey,
      unselectedItemColor: Colors.white,
    );
  }
}
