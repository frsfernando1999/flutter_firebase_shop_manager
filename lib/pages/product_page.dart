import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gerencia_loja/blocs/products_bloc.dart';
import 'package:gerencia_loja/validators/product_validator.dart';
import 'package:gerencia_loja/widgets/product_page_widgets/images_form_widget.dart';
import 'package:gerencia_loja/widgets/product_page_widgets/product_sizes.dart';

class ProductScreen extends StatefulWidget {
  final String? categoryId;
  final DocumentSnapshot? product;

  ProductScreen({this.product, required this.categoryId, Key? key})
      : super(key: key);

  @override
  State<ProductScreen> createState() =>
      _ProductScreenState(categoryId, product);
}

class _ProductScreenState extends State<ProductScreen> with ProductValidator {
  final ProductBloc _productBloc;
  final formKey = GlobalKey<FormState>();

  _ProductScreenState(String? categoryId, DocumentSnapshot? product)
      : _productBloc = ProductBloc(categoryId: categoryId, product: product);

  @override
  Widget build(BuildContext context) {
    _buildDecoration(String label) {
      return InputDecoration(
          labelText: label,
          labelStyle: const TextStyle(color: Colors.grey),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white, width: 1),
          ));
    }

    const fieldStyle = TextStyle(color: Colors.white, fontSize: 16);

    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        elevation: 0,
        title: StreamBuilder<bool>(
            stream: _productBloc.outCreated,
            initialData: false,
            builder: (context, snapshot) {
              return Text(snapshot.data! ? 'Editar Produto' : 'Criar Produto');
            }),
        actions: [
          StreamBuilder<bool>(
              stream: _productBloc.outCreated,
              initialData: false,
              builder: (context, snapshot) {
                if (snapshot.data!) {
                  return StreamBuilder<bool>(
                      stream: _productBloc.outLoading,
                      initialData: false,
                      builder: (context, snapshot) {
                        return IconButton(
                            onPressed: snapshot.data!
                                ? null
                                : () {
                                    _productBloc.deleteProduct();
                                    Navigator.of(context).pop();
                                  },
                            icon: const Icon(Icons.delete));
                      });
                } else {
                  return Container();
                }
              }),
          StreamBuilder<bool>(
              stream: _productBloc.outLoading,
              initialData: false,
              builder: (context, snapshot) {
                return IconButton(
                    onPressed: snapshot.data! ? null : saveProduct,
                    icon: const Icon(Icons.save));
              }),
        ],
      ),
      body: Stack(
        children: [
          Form(
            key: formKey,
            child: StreamBuilder<Map>(
                stream: _productBloc.outData,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Container();
                  return ListView(
                    padding: const EdgeInsets.all(16),
                    children: [
                      const Text(
                        'Imagens',
                        style: TextStyle(color: Colors.grey, fontSize: 12),
                      ),
                      ImagesFormWidget(
                        context: context,
                        initialValue: snapshot.data?['images'],
                        onSaved: _productBloc.saveImages,
                        validator: validateImages,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        initialValue: snapshot.data?['title'],
                        style: fieldStyle,
                        decoration: _buildDecoration('Título'),
                        onSaved: _productBloc.saveTitle,
                        validator: validateTitle,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        initialValue: snapshot.data?['description'],
                        style: fieldStyle,
                        maxLines: 6,
                        decoration: _buildDecoration('Descrição'),
                        onSaved: _productBloc.saveDescription,
                        validator: validateDescription,
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        initialValue:
                            snapshot.data?['preco']?.toStringAsFixed(2),
                        style: fieldStyle,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                              RegExp(r'^\d+\.?\d{0,2}')),
                        ],
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: true),
                        decoration: _buildDecoration('Preço'),
                        onSaved: _productBloc.savePrice,
                        validator: validatePrice,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      const Text(
                        'Tamanhos',
                        style: TextStyle(color: Colors.grey, fontSize: 12),
                      ),
                      ProductsSize(
                        context: context,
                          initialValue: snapshot.data?['sizes'],
                          onSaved: _productBloc.saveSizes,
                          validator: validateSize,
                          ),
                    ],
                  );
                }),
          ),
          StreamBuilder<bool>(
              stream: _productBloc.outLoading,
              initialData: false,
              builder: (context, snapshot) {
                return IgnorePointer(
                  ignoring: !snapshot.data!,
                  child: Container(
                    color: snapshot.data! ? Colors.black54 : Colors.transparent,
                  ),
                );
              }),
        ],
      ),
    );
  }

  Future<void> saveProduct() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Salvando produtos...',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          duration: Duration(minutes: 1),
          backgroundColor: Colors.pinkAccent,
        ),
      );

      bool success = await _productBloc.saveProduct();

      ScaffoldMessenger.of(context).removeCurrentSnackBar();

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            success ? 'Produto salvo!' : 'Erro ao salvar produto',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          duration: Duration(seconds: 2),
          backgroundColor: Colors.pinkAccent,
        ),
      );
    }
  }
}
