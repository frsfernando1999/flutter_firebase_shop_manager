import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

class UserBloc extends BlocBase {
  final _usersController = BehaviorSubject<List>();

  Stream<List> get outUsers => _usersController.stream;

  final Map<String, Map<String, dynamic>> _users = {};
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  UserBloc() {
    _addUserListener();
  }

  void onChangedSearch(String search) {
    if (search.trim().isEmpty) {
      _usersController.add(_users.values.toList());
    } else {
      _usersController.add(_filter(search.trim()));
    }
  }

  List<Map<String, dynamic>> _filter(String search) {
    List<Map<String, dynamic>> filterUsers = List.from(_users.values.toList());
    filterUsers.retainWhere((user) {
      return user['name'].toUpperCase().contains(search.toUpperCase()) ||
          user['email'].toUpperCase().contains(search.toUpperCase());
    });
    return filterUsers;
  }

  void _addUserListener() {
    _firestore.collection('users').snapshots().listen((snapshot) {
      for (var change in snapshot.docChanges) {
        String uid = change.doc.id;

        switch (change.type) {
          case DocumentChangeType.added:
            _users[uid] = change.doc.data() as Map<String, dynamic>;
            _subscribeToOrders(uid);
            break;

          case DocumentChangeType.modified:
            _users[uid]?.addAll(change.doc.data() as Map<String, dynamic>);
            _usersController.add(_users.values.toList());
            break;

          case DocumentChangeType.removed:
            _users.remove(uid);
            _unsubscribeToOrders(uid);
            _usersController.add(_users.values.toList());
            break;
        }
      }
    });
  }

  /* Este aplicativo atualiza as informações dos gastos das pessoas em tempo real,
  * o que, dependendo da quantidade de usuarios que este aplicativo pode ter,
  * pode o tornar MUITO pesado. É recomendado que caso você vá fazer um aplicativo
  * que irá ter muitos usuários, colocar apenas um botão que atualize a lista inteira,
  * evitando usar muitos listeners. */

  void _subscribeToOrders(String uid) {
    _users[uid]!['subscription'] = _firestore
        .collection('users')
        .doc(uid)
        .collection('orders')
        .snapshots()
        .listen((orders) async {
      int numOrders = orders.docs.length;

      double money = 0.0;

      for (DocumentSnapshot d in orders.docs) {
        DocumentSnapshot order =
            await _firestore.collection('orders').doc(d.id).get();

        if (order.data == null) {
          continue;
        }

        money += order['totalPrice'];
      }
      _users[uid]?.addAll({'money': money, 'orders': numOrders});
      _usersController.add(_users.values.toList());
    });
  }

  Map<String, dynamic>? getUser(String uid){
    return _users[uid];
  }

  void _unsubscribeToOrders(String uid) {
    _users[uid]!['subscription'].cancel();
  }

  @override
  void dispose() {
    _usersController.close();
  }
}
