import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

enum SortCriteria { concluded_first, concluded_last }

class OrdersBloc extends BlocBase {
  final _orderController = BehaviorSubject<List>();

  Stream<List> get outOrders => _orderController.stream;

  List<DocumentSnapshot> _orders = [];

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  SortCriteria? _criteria;

  OrdersBloc() {
    _addOrdersListener();
  }

  void _addOrdersListener() {
    _firestore.collection('orders').snapshots().listen((snapshot) {
      for (var change in snapshot.docChanges) {
        String oid = change.doc.id;

        switch (change.type) {
          case DocumentChangeType.added:
            _orders.add(change.doc);
            break;

          case DocumentChangeType.modified:
            _orders.removeWhere((order) => order.id == oid);
            _orders.add(change.doc);
            break;

          case DocumentChangeType.removed:
            _orders.removeWhere((order) => order.id == oid);
            break;
        }
      }
      _sort();
    });
  }

  void setOrderCriteria(SortCriteria criteria) {
    _criteria = criteria;

    _sort();
  }

  void _sort() {
    switch (_criteria) {
      case SortCriteria.concluded_first:
        _orders.sort((a, b) {
          int sa = a['status'];
          int sb = b['status'];
          if (sa < sb) {
            return 1;
          } else if (sa > sb) {
            return -1;
          } else {
            return 0;
          }
        });
        break;
      case SortCriteria.concluded_last:
        _orders.sort((a, b) {
          int sa = a['status'];
          int sb = b['status'];
          if (sa > sb) {
            return 1;
          } else if (sa < sb) {
            return -1;
          } else {
            return 0;
          }
        });
        break;
    }
    _orderController.add(_orders);
  }

  @override
  void dispose() {
    _orderController.close();
  }
}
