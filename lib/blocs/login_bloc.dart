import 'dart:async';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:gerencia_loja/validators/login_validator.dart';
import 'package:rxdart/rxdart.dart';

enum LoginState {idle, loading, success, fail}

class LoginBloc extends BlocBase with LoginValidators {
  final _emailControler = BehaviorSubject<String>();
  final _passControler = BehaviorSubject<String>();
  final _stateController = BehaviorSubject<LoginState>();

  Stream<String?> get outEmail =>
      _emailControler.stream.transform(validadeEmail);

  Stream<String?> get outPass => _passControler.stream.transform(validadePass);
  Stream<LoginState> get outState => _stateController.stream;

  Stream<bool> get outSubmitValid =>
      Rx.combineLatest2(outEmail, outPass, (a, b) => a != null && b != null);

  Function(String) get changeEmail => _emailControler.sink.add;
  Function(String) get changePass => _passControler.sink.add;
  StreamSubscription? _streamSubscription;

  LoginBloc() {
    _streamSubscription = FirebaseAuth.instance.authStateChanges().listen((user) async {
      if (user != null) {
        if(await verifyPrivileges(user)){
          _stateController.add(LoginState.success);
        }else{
          FirebaseAuth.instance.signOut();
          _stateController.add(LoginState.fail);
        }
      } else {
        _stateController.add(LoginState.idle);
      }
    });
  }

  void submit() {
    final email = _emailControler.value;
    final pass = _passControler.value;
    _stateController.add(LoginState.loading);

    FirebaseAuth.instance.signInWithEmailAndPassword(
      email: email,
      password: pass,
    ).catchError((onError) {
      _stateController.add(LoginState.fail);
    });
  }

  Future<bool> verifyPrivileges(User? user) async{
    return await FirebaseFirestore.instance
        .collection('admins')
        .doc(user?.uid)
        .get().then((doc){
        if(doc.data() != null){
          return true;
        }else{
          return false;
        }
    }).catchError((onError){
      return false;
    });
  }
  

  @override
  void dispose() {
    _emailControler.close();
    _passControler.close();
    _stateController.close();
    _streamSubscription?.cancel();
  }
}
